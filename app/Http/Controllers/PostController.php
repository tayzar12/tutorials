<?php

namespace App\Http\Controllers;

use App\Model\Post;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::allPosts();

        return view('posts.index', compact('posts'));
    }
}
