<?php

namespace App\Model;

use App\QueryFilters\Active;
use App\QueryFilters\Search;
use App\QueryFilters\Sort;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;

class Post extends Model
{
    protected $fillable = [
        'title',
        'active'
    ];

    public static function allPosts()
    {
        $pipes = [
            Active::class,
            Sort::class,
            Search::class
        ];

        return $posts = app(Pipeline::class)
            ->send(Post::query())
            ->through($pipes)
            ->thenReturn()
            ->paginate(15);
    }
}
