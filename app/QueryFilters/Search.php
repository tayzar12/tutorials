<?php

namespace App\QueryFilters;

class Search extends Filter
{
    protected function applyFilter($builder)
    {
        $keyword = request($this->filterName());
        return $builder->where('title', 'like', "%$keyword%");
    }
}
